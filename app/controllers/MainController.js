app.controller("MainController", function ($scope, $rootScope, $location) {
    'use strict';
    $scope.myVar = 12;

    $rootScope.goToRoute = function (path) {
        $location.path(path);
        if (path === '/') {
            $rootScope.isWelcomePage = true;
        } else {
            $rootScope.isWelcomePage = false;
        }
    };

});