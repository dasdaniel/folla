app.factory('resources', function () {
    'use strict';

    var resources = {};

    resources.spokenLanguages = [
        {
            name: 'English'
        },
        {
            name: 'Romanian'
        }
    ];

    return resources;

});