var app;

app = angular.module("Folla",  ['ngRoute']);

app.config(function ($routeProvider) {
    'use strict';

    $routeProvider
        .when('/', {
            templateUrl: 'welcome.html',
            controller: 'WelcomeController'
        })
        .when('/lessons', {
            templateUrl: 'lessons.html',
            controller: 'LessonsController'
        })
        .otherwise({
            redirectTo: '/'
        });
});
